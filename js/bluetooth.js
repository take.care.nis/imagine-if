var device = null;
var characteristic = null;
var server = null;

var connected = false;
var currentPartition = 0;

class Medicine {
    Name = "";
    Type = "";
    Condition = "";
    Partition = "";
    Days = [];
    Times = [];
}

var globalMedication = new Medicine();

var listOfMedicines = []

function connectOrDisconnect() {

    if (connected) {
        disconnect();
    }
    else {
        connect();
    }
}

async function connect() {

    try {
        device = await navigator.bluetooth.requestDevice({
            acceptAllDevices: true,
            optionalServices: ['0000ffe0-0000-1000-8000-00805f9b34fb']
        });

        server = await device.gatt.connect();
        let service = await server.getPrimaryService('0000ffe0-0000-1000-8000-00805f9b34fb');
        characteristic = await service.getCharacteristic('0000ffe1-0000-1000-8000-00805f9b34fb');

        characteristic.startNotifications();

        characteristic.addEventListener('characteristicvaluechanged', e => {
            let value = e.target.value;
            console.log(value);
            readValue(value);
        })

        if (server.connected) {
            connected = true;
            $("#bluetoothConnect").attr("src", "img/bluetoothon.png");

            Notification.requestPermission(result => {
                console.log(result);
            });

            validate();
        }
    }
    catch (err) {
    }
}

async function disconnect() {
    if (!device) {

        connected = false;
        $("#bluetoothConnect").attr("src", "img/bluetoothoff.png");
        return;
    }
    if (device.gatt.connected) {
        device.gatt.disconnect();

        connected = false;
        $("#bluetoothConnect").attr("src", "img/bluetoothoff.png");
        $("#partition1").css("opacity", "0.2");
        $("#partition2").css("opacity", "0.2");
        $("#partition3").css("opacity", "0.2");
        $("#partition4").css("opacity", "0.2");
        $("#partition5").css("opacity", "0.2");
        $("#partition6").css("opacity", "0.2");
        $("#partition7").css("opacity", "0.2");
        $("#partition8").css("opacity", "0.2");
    }
}


function validate() {
    if (!server.connected) {
        connect();
    }

    var currentDate = new Date();
    var currentTime = currentDate.getHours() + ":" + currentDate.getMinutes() + ":" + currentDate.getSeconds();
    characteristic.writeValue(str2ab("OK" + currentTime + "\\"));
}


function addNewMedicine() {

    var medicationName = $("#medicationInput").val();
    var medicationType = $("#medicationType").val();
    var medicationCondition = $("#medicationConditionInput").val();
    var medicationDays = $("#medicationDay").val();
    var medicationTimes = $("#medicationCountForDay").val();

    var number = parseInt(medicationTimes);
    var listOfTimes = [];

    for (var i = 1; i <= number; i++) {
        var timeAndNumber = {
            Time: $("#medicationCountForDayTime" + i).val(),
            Number: $("#medicationCountForDayNumber" + i).val()
        };

        listOfTimes.push(timeAndNumber);
    }

    var medication = new Medicine();
    medication.Name = medicationName;
    medication.Type = medicationType;
    medication.Condition = medicationCondition;
    medication.Days = medicationDays;
    medication.Times = listOfTimes;
    medication.Partition = currentPartition;

    globalMedication = medication;

    notifyPillBox(medication.Times);
}

function notifyPillBox(times) {

    var timesString = "";

    for (var i = 0; i < times.length; i++) {
        timesString += times[0].Time + ",";
    }

    timesString.replace(/.$/, '');

    if (!server.connected) {
        connect();
    }

    $("#addNewMedicineModal").modal("toggle")
    characteristic.writeValue(str2ab(timesString+"\\"));
}

function addMedicineName() {

    var medicine = new Medicine();
    medicine.Name = $("#medicationInput").val();

    listOfMedicines.push(medicine);
}

function readValue(value) {

    var stringValue = ab2str(value);

    if (stringValue.startsWith("OK")) {
        getPartitions(stringValue.substring(2));
    }
    else if (stringValue.indexOf(":") > -1) {
        timeForMedicine(stringValue);
    }
}

var timeForNotify = '';
var globalTimer = null;
function timeForMedicine(time) {

    var numberOfPills = null;
    var splitted = time.split('');
    var newTime = splitted[0] + splitted[2] + ":" + splitted[6] + splitted[8];

    timeForNotify = newTime;
    for (var i = 0; i < globalMedication.Times.length; i++) {
        if (globalMedication.Times[i].Time == newTime) {
            numberOfPills = globalMedication.Times[i].Number;
        }
    }

    Notification.requestPermission(result => {
        console.log(result);

        if ('Notification' in window) {
            navigator.serviceWorker.ready.then(registration => {
                registration.showNotification('Time for Meds', {
                    body: 'It is time! You need to take your medication!',
                    tag: 'vibration-sample',
                    icon: 'https://www.freepngimg.com/thumb/pills/27373-7-pills-transparent-image.png'
                });
            });
        }
    });

    $("#partitionsDetails").text(globalMedication.Partition);
    $("#medicationDetails").text(globalMedication.Name);
    $("#medicationTypeDetails").text(globalMedication.Type);

    $("#notifyModal").modal("toggle");
}

function getPartitions(value) {

    var partitionsArray = value.split('');

    for (var i = 0; i < partitionsArray.length - 1; i++) {
        switch (partitionsArray[i]) {
            case '1':
                $("#partition1").css("opacity", "1");
                break;
            case '2':
                $("#partition2").css("opacity", "1");
                break;
            case '3':
                $("#partition3").css("opacity", "1");
                break;
            case '4':
                $("#partition4").css("opacity", "1");
                break;
            case '5':
                $("#partition5").css("opacity", "1");
                break;
            case '6':
                $("#partition6").css("opacity", "1");
                break;
            case '7':
                $("#partition7").css("opacity", "1");
                break;
            case '8':
                $("#partition8").css("opacity", "1");
                break;
            default:
                break;
        }
    }
}

async function sendMessage() {

    var message = $("#inputSendMessage").val();

    if (!server.connected) {
        connect();
    }

    characteristic.writeValue(str2ab(message));
}

function ab2str(buf) {
    var enc = new TextDecoder("utf-8");

    return enc.decode(buf);
}

function str2ab(str) {
    var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    var bufView = new Uint16Array(buf);
    for (var i = 0, strLen = str.length; i < strLen; i++) {
        bufView[i] = str.charCodeAt(i);
    }
    return buf;
}

function showAddNewMedicineDiv() {

    $("#addNewMedicineModal").modal("toggle")
}

$("#partition1").on('click', function () {
    currentPartition = 1;
    showAddNewMedicineDiv();
});

$("#partition2").on('click', function () {
    currentPartition = 2;
    showAddNewMedicineDiv();

});

$("#partition3").on('click', function () {
    currentPartition = 3;
    showAddNewMedicineDiv();

});

$("#partition4").on('click', function () {
    currentPartition = 4;
    showAddNewMedicineDiv();

});

$("#partition5").on('click', function () {
    currentPartition = 5;
    showAddNewMedicineDiv();

});

$("#partition6").on('click', function () {
    currentPartition = 6;
    showAddNewMedicineDiv();

});

$("#partition7").on('click', function () {
    currentPartition = 7;
    showAddNewMedicineDiv();
});

$("#partition8").on('click', function () {
    currentPartition = 8;
    showAddNewMedicineDiv();
});

$("#medicationCountForDay").on('change', function () {
    var number = parseInt(this.value);

    var firstInput = document.getElementById("medicationCountForDayDiv");
    firstInput.innerHTML = null;

    for (var i = 0; i < number; i++) {
        var input = document.createElement("input");
        input.className = "form-control medication-input medicationCountForDayTime";
        input.type = "time";
        input.style.marginTop = "10px";
        input.id = "medicationCountForDayTime" + (i + 1);

        var inputNum = document.createElement("input");
        inputNum.className = "form-control medication-input medicationCountForDayNumber";
        inputNum.type = "number";
        inputNum.min = "1";
        inputNum.value = "1";
        inputNum.style.marginTop = "10px";
        inputNum.id = "medicationCountForDayNumber" + (i + 1);

        firstInput.append(input);
        firstInput.append(inputNum);
    }
});